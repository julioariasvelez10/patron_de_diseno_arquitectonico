public class CalculatorView : IObserver
{
    public void ShowResult(double result)
    {
        Console.WriteLine("Result: " + result);
    }

    public void Update(double result)
    {
        ShowResult(result);
    }
}
