﻿internal class Program
{
  static void Main(string[] args)
    {
        CalculatorModel model = new CalculatorModel();
        CalculatorView view = new CalculatorView();

        model.RegisterObserver(view);

        model.Add(5, 10);
    }
}
//1. El sujeto (CalculatorModel) tiene un estado interno que representa el resultado actual de la operación (por ejemplo, el valor del atributo result).

//2. El observador (CalculatorView) se registra para recibir actualizaciones del sujeto mediante el método RegisterObserver.

//3. Cuando ocurre una operación en la calculadora (por ejemplo, se suma o resta), el sujeto actualiza su estado y, en este caso, el atributo result con el nuevo valor de la operación.

//4. Después de actualizar su estado interno, el sujeto llama al método NotifyObservers para notificar a todos los observadores registrados (en este caso, solo hay uno, que es la vista) sobre el cambio.

//5. El observador (CalculatorView) implementa el método Update de la interfaz IObserver, que es invocado por el sujeto durante la notificación. En este método, el observador recibe el nuevo resultado actualizado del sujeto y lo muestra en la interfaz gráfica mediante el método ShowResult.