using System;
using System.Collections.Generic;

public class CalculatorModel
{
    private double result;
    private List<IObserver> observers = new List<IObserver>();

    public double Result
    {
        get { return result; }
        private set
        {
            if (result != value)
            {
                result = value;
                NotifyObservers();
            }
        }
    }

    public void Add(double x, double y)
    {
        Result = x + y;
    }

    // Resto de operaciones y métodos...

    public void RegisterObserver(IObserver observer)
    {
        observers.Add(observer);
    }

    public void UnregisterObserver(IObserver observer)
    {
        observers.Remove(observer);
    }

    private void NotifyObservers()
    {
        foreach (var observer in observers)
        {
            observer.Update(Result);
        }
    }
}
