public interface IObserver
{
    void Update(double result);
}
